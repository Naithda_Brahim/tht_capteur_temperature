#include <Stepper.h>


const int stepsPerRevolution = 200;  // change this to fit the number of steps per revolution
// for your motor

// initialize the stepper library on pins 8 through 11:
Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

#include "DHT.h"
#define DHTPIN 9
#define DHTTYPE DHT11
// Créer l'objet 'sensor" de type DHT:
DHT sensor(DHTPIN, DHTTYPE);
void setup() {
  Serial.begin(9600);// INitiating Serial com
  sensor.begin();// Let start the sensor
  myStepper.setSpeed(60);

}

void loop() {
  delay(500);
  float h = sensor.readHumidity(); //Reading humidity
  float t = sensor.readTemperature(); //Reading temperature
  //Sending humidity,temperature to Serial port
  Serial.print("H:");
  Serial.print(h);
  Serial.print(",");
  Serial.print("T:");
  Serial.println(t);
  Serial.println("clockwise");
  myStepper.step(stepsPerRevolution);
  delay(500);

  // step one revolution in the other direction:
  Serial.println("counterclockwise");
  myStepper.step(-stepsPerRevolution);
  delay(500);

}
